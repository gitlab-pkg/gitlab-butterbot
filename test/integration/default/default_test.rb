# encoding: utf-8

# InSpec tests for gitlab-butterbot package

control 'general-pkg-checks' do
  impact 1.0
  title 'General tests for gitlab-butterbot package'
  desc "
    This control ensures that:
      * gitlab-butterbot package version 0.0.5 is installed
  "
  describe package('gitlab-butterbot') do
    it { should be_installed }
    its ('version') { should eq '0.0.5' }
  end
end

control 'general-service-checks' do
  impact 1.0
  title 'General tests for butterbot service'
  desc '
    This control ensures that:
      * butterbot service is installed, enabled and running
      * butterbot is listening on 0.0.0.0:3000
  '
  describe service('butterbot') do
    it { should be_installed }
    it { should be_enabled }
    it { should be_running }
  end

  describe port('3000') do
    its('processes') { should eq ['butterbot'] }
    its('protocols') { should eq ['tcp'] }
    its('addresses') { should eq ['::'] }
  end
end

control 'butterbot-checks' do
  impact 1.0
  title 'butterbot tests as a service'
  desc '
    This control ensures that:
      * butterbot service is returning metrics on port 3000
  '
  describe bash('curl -sSL http://127.0.0.1:3000/') do
    its('exit_status') { should eq 0 }
    its('stdout.strip') { should match (/404 page not found/) }
    its('stderr.strip') { should eq '' }
  end
end

control 'butterbot-requirements' do
  impact 1.0
  title 'butterbot requirements'
  desc '
    This control is just a collection of checks/reminders for
    humans that will request butter. Stuff that butterbot
    expects to be on system:
      * file with environment variables
      * nginx key and cert
  '
  describe file('/etc/butterbot.env') do
    it { should be_file }
    # owned and readable only by butterbot
    its('owner') {should eq 'butterbot' }
    its('group') {should eq 'butterbot' }
    its('mode') { should eq 0o400 }
  end

  describe file('/etc/ssl/private/butterbot.key') do
    it { should be_file }
    # owned and readable only by root
    its('owner') {should eq 'root' }
    its('group') {should eq 'root' }
    its('mode') { should eq 0o400 }
  end

  describe file('/etc/ssl/certs/butterbot.crt') do
    it { should be_file }
    # owned and readable only by root
    its('owner') {should eq 'root' }
    its('group') {should eq 'root' }
    its('mode') { should eq 0o400 }
  end

  describe file('/etc/nginx/sites-available/butterbot') do
    it { should be_file }
    # owned and readable only by root
    its('owner') {should eq 'root' }
    its('group') {should eq 'root' }
    its('mode') { should eq 0o400 }
  end

  # check config validity too
  describe bash('nginx -t -q') do
    its('exit_status') { should eq 0 }
    its('stdout.strip') { should eq '' }
    its('stderr.strip') { should eq '' }
  end

  describe file('/etc/nginx/sites-enabled/butterbot') do
    it { should be_symlink }
    it { should be_linked_to '/etc/nginx/sites-available/butterbot' }
  end
end

control 'nginx-checks' do
  impact 1.0
  title 'nginx checks'
  desc '
    This control ensures that:
      * nginx-core is installed
      * nginx is running on 0.0.0.0:443 (ipv4 only)
      * https requests pass to butterbot
  '

  describe package('nginx-core') do
    it { should be_installed }
  end

  describe service('nginx') do
    it { should be_installed }
    it { should be_enabled }
    it { should be_running }
  end

  describe port('443') do
    its('processes') { should eq ['nginx'] }
    its('protocols') { should eq ['tcp'] }
    its('addresses') { should eq ['0.0.0.0'] }
  end

  # same reply as direct access
  describe bash('curl -sSL https://butterbot/') do
    its('exit_status') { should eq 0 }
    its('stdout.strip') { should match (/404 page not found/) }
    its('stderr.strip') { should eq '' }
  end
end
