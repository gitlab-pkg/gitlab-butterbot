#!/bin/bash -x
# vim: ai:ts=8:sw=8:noet
# This is shell provisioner for Kitchen
set -eufo pipefail
IFS=$'\t\n'
VERSION="0.0.5"

# since gitlab-vault already in repo, install it from there
# and then install the version being tested with dpkg
# lazy way to test repo and fetch all dependencies
echo 'deb http://aptly.gitlab.com/ci xenial main' > /etc/apt/sources.list.d/aptly.gitlab.com.list
# TODO: a proper ssl from a proper CA, but for now just embed the fp here
wget -O /tmp/apt.key http://aptly.gitlab.com/release.asc
echo 'f4fd8d5bc50ab62b42ea6092867a61a9cfe2961c8629ec4591396c7e6e7ce7ab  /tmp/apt.key' | sha256sum --status --check
# shellcheck disable=SC2181
if [[ $? -ne 0 ]]; then
	echo "MiTM in progress, refusing to continue"
	exit $?
fi

apt-key add /tmp/apt.key
apt-get -qq update
dpkg -i /tmp/kitchen/data/gitlab-butterbot_${VERSION}.deb || true	# let it fail if no deps
## install dependencies
apt-get -yqqf install

## prepare config as human would do
# certs
openssl req \
	-new \
	-newkey rsa:2048 \
	-days 1 \
	-nodes \
	-x509 \
	-keyout /etc/ssl/private/butterbot.key \
	-out /etc/ssl/certs/butterbot.crt \
	-subj '/CN=butterbot/'
# not strictly required
chmod 0400 /etc/ssl/private/butterbot.key /etc/ssl/certs/butterbot.crt

# create nginx sample config -- see postinst

# just so that tests can resolve it
echo '127.0.0.1 butterbot' >> /etc/hosts

# reload nginx
systemctl reload nginx
